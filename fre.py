import telebot
from telebot import types

token = '455667840:AAHOUTLdSTr7KsKtFbnKVlCyfe9Q3whtc0o'

telebot.apihelper.proxy = {'https': 'socks5://goto:gotogotogoto@tvorog.me:6666'}

bot = telebot.TeleBot(token=token)

goods = [{'title': 'bread', 'price': '20'},
         {'title': 'meat', 'price': '50'},
         {'title': 'apple', 'price': '10'},
         {'title': 'button', 'price': '9000'},
         {'title': 'six', 'price': '100'}]

offset = 0


@bot.message_handler(commands=["start"])
def shop(message):
    keyboard = types.InlineKeyboardMarkup()
    for good in goods:
        name = good['title']
        price = good['price']
        for i in range(offset, offset + 4):
            button1 = types.InlineKeyboardButton(text="{} {} GT".format(name, price),                                      callback_data='button1')
            keyboard.add(button1)
    button_back = telebot.types.InlineKeyboardButton(text="Назад", callback_data='back')
    button_forward = telebot.types.InlineKeyboardButton(text="Вперёд", callback_data='forward')
    if offset - 4 > 0:
        keyboard.add(button_back)
    if offset + 4 < len(goods):
        keyboard.add(button_forward)
    bot.send_message(message.chat.id, "Выбирите товар:", reply_markup=keyboard)


def get_confirm_keyboard(message):
    bot.send_message(message.chat.id, "Вы хотите купить эту услугу?")
    keyboard = telebot.types.InlineKeyboardMarkup()
    yes_button = telebot.types.InlineKeyboardButton(text="Да", callback_data="yes")
    no_button = telebot.types.InlineKeyboardButton(text="Нет", callback_data="no")
    keyboard.add(yes_button)
    keyboard.add(no_button)
    return keyboard


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    global offset
    if call.message:
        if call.data == "button1":
            get_confirm_keyboard(message)
        if call.data == "back":
            offset -= 4
            shop()
        if call.data == "forward":
            offset += 4
            shop()


bot.polling(none_stop=True)
